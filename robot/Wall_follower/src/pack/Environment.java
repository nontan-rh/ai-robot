package pack;

public class Environment {
	private final float distance;
	private final boolean bumper;
	
	public Environment(float distance, boolean bumper) {
		this.distance = distance;
		this.bumper = bumper;
	}
	
	public final float getDistance() {
		return distance;
	}
	
	public final boolean getBumper() {
		return bumper;
	}
}
