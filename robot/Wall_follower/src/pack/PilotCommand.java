package pack;

public enum PilotCommand {
	STOP,
	SOFT_FORWARD,
	HARD_FORWARD,
	SOFT_LEFT,
	HARD_LEFT,
	SOFT_RIGHT,
	HARD_RIGHT,
}
