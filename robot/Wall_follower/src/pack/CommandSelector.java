package pack;

public class CommandSelector {
	private static final int NUM_BUMPER_STATE = 2;
	private static final int NUM_DISTANCE_STATE = 3;
	private static final int NUM_COMMANDS = 6;
	
	private float[] data;
	private boolean nearOnce;
	private Environment prevEnv;
	private Environment currEnv;
	
	public CommandSelector(float[] data, Environment env) {
		this.data = data;
		this.prevEnv = env;
		this.currEnv = env;
		this.nearOnce = false;
	}
	
	public void pushEnvironment(Environment env) {
		prevEnv = currEnv;
		currEnv = env;
	}
	
	private int classifyDistance(float distance) {
		if (distance < 0.6) {
			return 0;
		} else {
			return 1;
		}
	}
	
	public PilotCommand selectCommand() {
		int currBumper = currEnv.getBumper() ? 0 : 1;
		int currDist = classifyDistance(currEnv.getDistance());
		
		if (currEnv.getDistance() < 0.5) {
			nearOnce = true;
		}
		
		int stateIndex = currBumper + (nearOnce ? 2 : 0) + 4 * currDist;
		
		int dataIndexStart = NUM_COMMANDS * stateIndex;
		int dataIndexEnd = NUM_COMMANDS * (stateIndex + 1);
		
		float sum = 0;
		for (int i = dataIndexStart; i < dataIndexEnd; i++) {
			sum += data[i];
		}
		
		float randomNumber = (float)Math.random();
		
		float accum = 0;
		for (int i = dataIndexStart; i < dataIndexEnd; i++) {
			accum += data[i];
			
			if (randomNumber < accum / sum) {
				return PilotCommand.values()[i - dataIndexStart + 1];
			}
		}
		
		return PilotCommand.STOP;
	}
}
