package pack;

import pack.Learning;
import pack.QTools;
import lejos.hardware.Brick;
import lejos.hardware.BrickFinder;
import lejos.hardware.lcd.LCD;
import lejos.hardware.motor.Motor;
import lejos.hardware.port.Port;
import lejos.hardware.sensor.EV3TouchSensor;
import lejos.hardware.sensor.EV3UltrasonicSensor;
import lejos.robotics.SampleProvider;
import lejos.robotics.navigation.MovePilot;

public class Seeker {
	public static byte NUMBER_ACTIONS = 6;
	public static byte NUMBER_STATES = 4;

	static Brick brick;
	static Port s1;
	static Port s2;
	static MovePilot pilot;
	static EV3TouchSensor touchSensor;
	static EV3UltrasonicSensor ultrasonicSensor;
	static CommandSelector commandSelector;
	
	byte motorCommand = 0;
	
	static QTools q = new QTools(NUMBER_ACTIONS, NUMBER_STATES);
	static Learning Q = new Learning(NUMBER_ACTIONS, NUMBER_STATES, q);

	@SuppressWarnings("deprecation")
	public static void main(String[] args) {
		brick = BrickFinder.getDefault();
		pilot = new MovePilot(6, 13, Motor.B, Motor.C);
		pilot.setLinearSpeed(15);
				
		s1 = (Port) brick.getPort("S1");
		s2 = (Port) brick.getPort("S2");
		
		touchSensor = new EV3TouchSensor(s1);
		ultrasonicSensor = new EV3UltrasonicSensor(s2);
		
		double[] dataD = new double[] {
				0.8508063938306563,0.9039524394904848,0.04283118437799782,0.1464393539597355,0.04468052280249357,0.17943105630757095,0.8366200552623142,0.7296645729507374,0.01086755169381437,0.5431110523568048,0.013770031483686607,0.11175469940340732,0.5758300403104499,0.08263008549157358,0.8297723179124032,0.07921851162298221,0.9954751813281246,0.021774641666693295,0.9830019371248535,0.368102662425303,0.8558535908596496,0.22108078440284995,0.7525862751334069,0.7550923408548949,0.9603648235349067,0.9716691067514911,0.2029642844424442,0.2769625281795005,0.17626420873936643,0.027514762515973512,0.34263615407209147,0.24227092713247056,0.0018938738546889766,0.09211285983266704,0.7562568393542917,0.8938600551110027,0.778013671138815,0.8524996775790099,0.6003482717294499,0.3484787695403644,0.08866678537826789,0.9057421648089543,0.8575968950842874,0.29044626882432967,0.4880582452235218,0.545255257841367,0.7418333485686059,0.6994767974905194
		};
		float[] data = new float[dataD.length];
		for (int i = 0; i < dataD.length; i++) {
			data[i] = (float)dataD[i];
		}
		commandSelector = new CommandSelector(data, getEnvironment());
		
		final int angle = 50;
		final int sleepTimeMillis = 500;
		final int numIteration = 100;
		try {
			for (int i = 0; i < numIteration; i++) {
				try {
					// Motor.A.rotate(angle * (i % 2 == 0 ? 1 : -1));
					Thread.sleep(sleepTimeMillis);
					
					Environment e = getEnvironment();
					commandSelector.pushEnvironment(e);
					PilotCommand command = commandSelector.selectCommand();
					LCD.clear();
					LCD.drawString(command.name(), 0, 0);
					executePilotCommand(command);
				} catch (Exception e) {
				}
			}
		} finally {
			touchSensor.close();
			ultrasonicSensor.close();
		}
	}

	public static void executePilotCommand(PilotCommand command) {
		MovePilot p = pilot;
		
		final float SOFT_ANGLE = 10;
		final float HARD_ANGLE = 20;
		switch(command) {
		case STOP:
			p.stop();
			break;
		case SOFT_LEFT:
			p.rotate(SOFT_ANGLE);
			break;
		case HARD_LEFT:
			p.rotate(HARD_ANGLE);
			break;
		case SOFT_RIGHT:
			p.rotate(-SOFT_ANGLE);
			break;
		case HARD_RIGHT:
			p.rotate(-HARD_ANGLE);
			break;
		case SOFT_FORWARD:
			p.forward();
			break;
		case HARD_FORWARD:
			p.forward();
			break;
		}
	}
	
	public static float[] getSamples(SampleProvider sp) {
		int sampleSize = sp.sampleSize();
		float[] samples = new float[sampleSize];
		sp.fetchSample(samples, 0);
		return samples;
	}
	
	public static Environment getEnvironment() {
		SampleProvider touchSensorProvider = touchSensor.getTouchMode();
		SampleProvider ultrasonicSensorProvider = ultrasonicSensor.getDistanceMode();
		
		float[] touchSensorSamples = getSamples(touchSensorProvider);
		float[] ultrasonicSensorSamples = getSamples(ultrasonicSensorProvider);
		
		boolean bumper = touchSensorSamples[0] == 0;
		float distance = ultrasonicSensorSamples[0];
		
		return new Environment(distance, bumper);
	}
	
	public static void showSensor(SampleProvider sp, int lcdYOffset) {
		float[] samples = getSamples(sp);
		for (int i = 0; i < samples.length; i++) {
			float value = samples[i];
			LCD.drawString(String.valueOf(value), 0, lcdYOffset + i);
		}
	}
}