import { vec2, mat2 } from 'gl-matrix';
import { CommandSelector } from './CommandSelector';
import { Environment } from './Environment';
import { Field } from './Field';
import { Command } from './Command';
import { lerp } from './Util';

const identity = mat2.create();

const forward0Distance = 0.02;
const forward1Distance = 0.04;
const bumperTestDistance = 0.01;
const turnLeft0Mat = mat2.rotate(mat2.create(), identity, 10.0 / 180 * Math.PI);
const turnLeft1Mat = mat2.rotate(mat2.create(), identity, 20.0 / 180 * Math.PI);
const turnLeft2Mat = mat2.rotate(mat2.create(), identity, -10.0 / 180 * Math.PI);
const turnLeft3Mat = mat2.rotate(mat2.create(), identity, -20.0 / 180 * Math.PI);

export class Robot {
  pos: vec2;
  dir: vec2;
  envionment: Environment;
  commandSelector: CommandSelector;

  moveOneStep = (() => {
    const p = vec2.create();
    const d = vec2.create();
    const n = vec2.create();
    const t = vec2.create();
    const c = vec2.create();

    return (field: Field) => {
      vec2.copy(p, this.pos);
      vec2.copy(d, this.dir);

      const command = this.commandSelector.selectCommand(this.envionment);
      switch (command) {
        case Command.Stop:
          break;
        case Command.Forward0:
          vec2.scaleAndAdd(n, this.pos, this.dir, forward0Distance);
          if (field.checkCollision(c, this.pos, n)) {
            vec2.copy(this.pos, c);
          } else {
            vec2.copy(this.pos, n);
          }
          break;
        case Command.Forward1:
          vec2.scaleAndAdd(n, this.pos, this.dir, forward1Distance);
          if (field.checkCollision(c, this.pos, n)) {
            vec2.copy(this.pos, c);
          } else {
            vec2.copy(this.pos, n);
          }
          break;
        case Command.TurnLeft0:
          vec2.transformMat2(this.dir, d, turnLeft0Mat);
          break;
        case Command.TurnLeft1:
          vec2.transformMat2(this.dir, d, turnLeft1Mat);
          break;
        case Command.TurnLeft2:
          vec2.transformMat2(this.dir, d, turnLeft2Mat);
          break;
        case Command.TurnLeft3:
          vec2.transformMat2(this.dir, d, turnLeft3Mat);
          break;
        default:
          throw new Error('Unknown command');
      }

      vec2.scaleAndAdd(t, this.pos, this.dir, bumperTestDistance);
      if (field.checkCollision(c, this.pos, t)) {
        this.envionment.bumper = true;
      } else {
        this.envionment.bumper = false;
      }

      this.envionment.distance = field.measureTarget(this.pos, this.dir);
      this.envionment.nearOnce = this.envionment.nearOnce || this.envionment.distance < 0.4;
    };
  })();

  render = (() => {
    const tip = vec2.create();

    return (ctx: CanvasRenderingContext2D, width: number, height: number) => {
      ctx.lineWidth = 2;
      ctx.fillStyle = 'transparent';

      const left = 0.1 * width;
      const top = 0.1 * height;
      const right = 0.9 * width;
      const bottom = 0.9 * height;

      vec2.scaleAndAdd(tip, this.pos, this.dir, 0.1);
      const tipXPx = lerp(left, right, tip[0]);
      const tipYPx = lerp(top, bottom, tip[1]);
      const posXPx = lerp(left, right, this.pos[0]);
      const posYPx = lerp(top, bottom, this.pos[1]);
      const radius = 10;

      if (this.envionment.bumper) {
        ctx.strokeStyle = 'red';
      } else {
        ctx.strokeStyle = 'blue';
      }

      ctx.beginPath();
      ctx.arc(posXPx, posYPx, radius, 0, 2 * Math.PI, false);
      ctx.stroke();

      if (this.envionment.distance > 1.0) {
        ctx.strokeStyle = 'blue';
      } else {
        ctx.strokeStyle = 'green';
      }

      ctx.beginPath();
      ctx.moveTo(posXPx, posYPx);
      ctx.lineTo(tipXPx, tipYPx);
      ctx.stroke();
    };
  })();

  constructor(initialPos: vec2, initialDir: vec2, commandSelector: CommandSelector) {
    this.pos = vec2.create();
    this.dir = vec2.create();
    vec2.copy(this.pos, initialPos);
    vec2.copy(this.dir, initialDir);

    this.envionment = new Environment();
    this.commandSelector = commandSelector;
  }
}