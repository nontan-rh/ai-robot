export enum Command {
  Stop = 0,
  Forward0,
  Forward1,
  TurnLeft0,
  TurnLeft1,
  TurnLeft2,
  TurnLeft3,
  UPPER_BOUND,
}