import { vec2 } from 'gl-matrix';
import { cross, lerp } from './Util';

const obstacleTop = 0.40;
const obstacleBottom = 0.60;
const obstacleRight = 0.50;

export class Field {
  lines: [vec2, vec2][] = [
    [vec2.fromValues(0, 0), vec2.fromValues(1, 0)],
    [vec2.fromValues(1, 0), vec2.fromValues(1, 1)],
    [vec2.fromValues(1, 1), vec2.fromValues(0, 1)],
    [vec2.fromValues(0, 1), vec2.fromValues(0, obstacleBottom)],
    [vec2.fromValues(0, obstacleBottom), vec2.fromValues(obstacleRight, obstacleBottom)],
    [vec2.fromValues(obstacleRight, obstacleBottom), vec2.fromValues(obstacleRight, obstacleTop)],
    [vec2.fromValues(obstacleRight, obstacleTop), vec2.fromValues(0, obstacleTop)],
    [vec2.fromValues(0, obstacleTop), vec2.fromValues(0, 0)],
  ];

  targetPos: vec2;

  checkCollision = (() => {
    const epsilon = .000001;
    const a1a2 = vec2.create();
    const a1b1 = vec2.create();
    const b1a1 = vec2.create();
    const a1b2 = vec2.create();
    const b1b2 = vec2.create();
    const b1a2 = vec2.create();

    return (r: vec2, a1: vec2, a2: vec2): boolean => {
      for (const [b1, b2] of this.lines) {
        vec2.sub(a1a2, a2, a1);
        vec2.sub(a1b1, b1, a1);
        vec2.sub(b1a1, a1, b1);
        vec2.sub(a1b2, b2, a1);
        vec2.sub(b1b2, b2, b1);
        vec2.sub(b1a2, a2, b1);

        if (cross(a1a2, a1b1) * cross(a1a2, a1b2) < epsilon
          && cross(b1b2, b1a1) * cross(b1b2, b1a2) < epsilon
          && cross(a1a2, b1b2) >= 0) {

          const d1 = Math.abs(cross(b1b2, b1a1));
          const d2 = Math.abs(cross(b1b2, b1a2));

          let t = d1 / (d1 + d2);
          if (Number.isNaN(t)) {
            t = 0;
          }

          vec2.scaleAndAdd(r, a1, a1a2, t * 0.99);
          return true;
        }
      }

      return false;
    };
  })();

  measureTarget = (() => {
    const robotToTarget = vec2.create();
    const robotToTargetN = vec2.create();
    const angleCos = Math.cos(25.0 / 180 * Math.PI);

    return (pos: vec2, dir: vec2): number => {
      vec2.sub(robotToTarget, this.targetPos, pos);
      vec2.normalize(robotToTargetN, robotToTarget);

      if (vec2.dot(robotToTargetN, dir) > angleCos) {
        return vec2.len(robotToTarget);
      } else {
        return Number.POSITIVE_INFINITY;
      }
    };
  })();

  constructor(targetPos: vec2) {
    this.targetPos = vec2.create();
    vec2.copy(this.targetPos, targetPos);
  }

  getDistanceToTarget(x: number, y: number) {
    return Math.sqrt((this.targetPos[0] - x) ** 2 + (this.targetPos[1] - y) ** 2);
  }

  render(ctx: CanvasRenderingContext2D, width: number, height: number) {
    ctx.lineWidth = 2;
    ctx.fillStyle = 'transparent';
    ctx.strokeStyle = 'black';

    const left = 0.1 * width;
    const top = 0.1 * height;
    const right = 0.9 * width;
    const bottom = 0.9 * height;

    ctx.beginPath();
    for (const [b1, b2] of this.lines) {
      const b1x = lerp(left, right, b1[0]);
      const b1y = lerp(top, bottom, b1[1]);
      const b2x = lerp(left, right, b2[0]);
      const b2y = lerp(top, bottom, b2[1]);
      ctx.moveTo(b1x, b1y);
      ctx.lineTo(b2x, b2y);
    }
    ctx.stroke();

    ctx.strokeStyle = 'green';

    const posXPx = lerp(left, right, this.targetPos[0]);
    const posYPx = lerp(top, bottom, this.targetPos[1]);
    const radius = 10;

    ctx.beginPath();
    ctx.arc(posXPx, posYPx, radius, 0, 2 * Math.PI, false);
    ctx.stroke();
  }
}