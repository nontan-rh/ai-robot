import { vec2 } from 'gl-matrix';

export function lerp(a: number, b: number, t: number) {
  return a * (1 - t) + b * t;
}

export function cross(l: vec2, r: vec2) {
  return l[0] * r[1] - l[1] * r[0];
}
