export class Environment {
  bumper: boolean;
  distance: number;
  nearOnce: boolean;

  constructor(bumper: boolean = false, distance: number = Number.POSITIVE_INFINITY) {
    this.bumper = bumper;
    this.distance = distance;
    this.nearOnce = false;
  }

  getDistanceClass() {
    if (this.distance < 0.8) {
      return 0;
    } else {
      return 1;
    }
  }
}