import { Environment } from './Environment';
import { Command } from './Command';

export class CommandSelector {
  data: number[];

  // bumper: true -> 1, false -> 0

  constructor(data: number[]) {
    this.data = data.slice();
  }

  selectCommand(env: Environment): Command {
    const numCommands = Command.UPPER_BOUND - 1;

    const envIndex = (env.bumper ? 1 : 0) + (env.nearOnce ? 2 : 0) + 4 * env.getDistanceClass();
    const indexStart = numCommands * envIndex;
    const indexEnd = numCommands * (envIndex + 1);

    if (indexEnd > this.data.length) {
      throw new Error('Index out of range');
    }
   
    let sum = 0;
    for (let i = indexStart; i < indexEnd; i++) {
      sum += this.data[i];
    }

    const randomNumber = Math.random();
    let accum = 0;
    for (let i = indexStart; i < indexEnd; i++) {
      accum += this.data[i];

      const probability = accum / sum;
      if (randomNumber <= probability) {
        return i - indexStart + 1;
      }
    }

    return Command.Stop;
  }
}