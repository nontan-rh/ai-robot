import * as React from 'react';
import './App.css';
import { Context } from 'vm';
import { Field } from './Simulator/Field';
import { vec2 } from 'gl-matrix';
import { Robot } from './Simulator/Robot';
import { CommandSelector } from './Simulator/CommandSelector';
import { Learning } from './GA/Learning';

interface State {
  isRunning: boolean;
  sleepLength: number;
  width: number;
  height: number;
  iterationLogs: string[];
}

class App extends React.Component<{}, State> {
  field: Field;
  robot: Robot;
  learning: Learning;
  canvas: HTMLCanvasElement;
  context: CanvasRenderingContext2D;

  constructor(props: {}, context: Context) {
    super(props, context);

    this.state = {
      isRunning: true,
      sleepLength: 100,
      width: 600,
      height: 600,
      iterationLogs: [],
    };

    // tslint:disable-next-line:max-line-length
    const data: number[] = [0.17635977308947015, 0.998319635002632, 0.40812274599913856, 0.5973522632947477, 0.038890729222187836, 0.287924670508664, 0.12599399205652184, 0.9773481427459145, 0.3076780253410878, 0.25333273697689385, 0.5018574583427227, 0.18879176371633166, 0.45744731443573583, 0.9976116441221756, 0.20672748536677954, 0.7145112278883297, 0.059245676967822236, 0.2362827064720492, 0.5208027506980779, 0.15872982295867932, 0.23481569358419563, 0.49710841156843344, 0.021084171256426254, 0.0627619160516617, 0.08132258503517154, 0.9517954400859647, 0.02670343369515704, 0.001992447303623912, 0.10359823012393576, 0.2096391373939206, 0.0207719658112262, 0.023123878565676215, 0.05655047466145624, 0.9261353728002173, 0.048667307314610886, 0.017454010390583363, 0.8125318039930476, 0.7169011297998271, 0.2460788184493854, 0.24621886126980885, 0.6160315180007749, 0.25307260022295885, 0.570716982217037, 0.3773201987792971, 0.4219816013259574, 0.5016375414775707, 0.9563165260521529, 0.9476885776547559];
    const commandSelector = new CommandSelector(data);

    const upperPos = vec2.fromValues(0.1, 0.8);
    const lowerPos = vec2.fromValues(0.1, 0.2);
    const initialDir = vec2.fromValues(1.0, 0.0);

    const lowerField = new Field(lowerPos);
    const upperField = new Field(upperPos);

    const timePenaltyFactor = 3;
    this.field = lowerField;
    this.robot = new Robot(upperPos, initialDir, commandSelector);
    this.learning = new Learning([{ 
      field: lowerField,
      initialPos: upperPos,
      initialDir,
      getScore(t: number, x: number, y: number) {
        const distanceFactor = Math.max(0.8 - lowerField.getDistanceToTarget(x, y), 0);
        const yFactor = 1. - Math.max(y - 0.4, 0) / 0.6;
        return (distanceFactor + yFactor * 2) * (t + timePenaltyFactor) / (1 + timePenaltyFactor);
      }
    }, {
      field: upperField,
      initialPos: lowerPos,
      initialDir,
      getScore(t: number, x: number, y: number) {
        const distanceFactor = Math.max(0.8 - upperField.getDistanceToTarget(x, y), 0);
        const yFactor = 1. - Math.max(0.6 - y, 0) / 0.6;
        return (distanceFactor + yFactor * 2) * (t + timePenaltyFactor) / (1 + timePenaltyFactor);
      }
    }]);
  }

  componentDidMount() {
    const context = this.canvas.getContext('2d');
    if (context === null) {
      throw new Error();
    }
    this.context = context;

    if (this.state.isRunning) {
      requestAnimationFrame(this.onAnimationFrame);
    }
  }

  onAnimationFrame = () => {
    this.robot.moveOneStep(this.field);

    this.context.clearRect(0, 0, this.state.width, this.state.height);

    this.field.render(this.context, this.state.width, this.state.height);
    this.robot.render(this.context, this.state.width, this.state.height);

    if (this.state.isRunning) {
      if (this.state.sleepLength < 16) {
        requestAnimationFrame(this.onAnimationFrame);
      } else {
        setTimeout(() => { requestAnimationFrame(this.onAnimationFrame); }, this.state.sleepLength);
      }
    }
  }

  onClickStart = async () => {
    const numGenerations = 200;
    for (let i = 0; i < numGenerations; i++) {
      const result = this.learning.calcNextGen();

      // tslint:disable-next-line:max-line-length
      const log = `episode: ${i}\nave: ${result.scoreAve}\nmax: ${result.scoreMax}\nelite: ${JSON.stringify(result.scoreMaxGene)}`;
      console.log(log);

      const iterationLogs = this.state.iterationLogs.slice();
      iterationLogs.push(log);
      this.setState({ iterationLogs });
      // await new Promise(resolve => setTimeout(() => resolve(), 1000));
    }
  }

  render() {
    return (
      <div className="App">
        <canvas
          id="Canvas"
          ref={x => { this.canvas = x as HTMLCanvasElement; }}
          // tslint:disable-next-line:jsx-alignment
          width={this.state.width + 'px'} height={this.state.height + 'px'} />
        <div className="Control">
          <div>
            <button onClick={this.onClickStart} >Start</button>
          </div>
          <div className="IterationLogContainer">
            {this.state.iterationLogs.map(log => (<div>{log}</div>))}
          </div>
        </div>
      </div>
    );
  }
}

export default App;
