import { getRandomData, getRandomInt } from './Util';
import { Field } from '../Simulator/Field';
import { Robot } from '../Simulator/Robot';
import { vec2 } from 'gl-matrix';
import { CommandSelector } from '../Simulator/CommandSelector';

const numGenes = 400;
const numIteration = 200;
const numTicks = 150; // How many commands per iteration

const selectionProbability = 0.10;
const mutationProbability = 0.03;

interface TestSet {
  field: Field;
  initialPos: vec2;
  initialDir: vec2;
  getScore(t: number, x: number, y: number): number;
}

export class Learning {
  testSets: TestSet[];

  pool: number[][];

  constructor(testSets: TestSet[]) {
    this.testSets = testSets;

    this.pool = new Array();
    for (let i = 0; i < numGenes; i++) {
      this.pool.push(getRandomData());
    }
  }

  calcNextGen() {
    const scores = this.testGeneAll();
    let nextGen: number[][] = [];

    const selectNum = Math.floor(scores.length * selectionProbability);

    const list: [number, number[]][] = [];
    for (let i = 0; i < scores.length; i++) {
      list.push([scores[i], this.pool[i]]);
    }
    list.sort((a, b) => b[0] - a[0]);
    for (let i = 0; i < selectNum; i++) {
      nextGen.push(list[i][1]);
    }

    for (let i = 0; i < numGenes - selectNum; i++) {
      const x = Math.random();
      let t = 0;

      t += mutationProbability;
      if (x < t) {
        // Mutation
        const g = this.pool[getRandomInt(0, numGenes)];
        const mg = this.mutateGene(g);
        nextGen.push(mg);
        continue;
      }

      // Crossing
      const g1 = this.pool[getRandomInt(0, numGenes)];
      const g2 = this.pool[getRandomInt(0, numGenes)];
      nextGen.push(this.crossGenes(g1, g2));
    }

    let scoreSum = 0;
    let scoreMax = 0;
    let scoreMaxGene: number[] = [];
    for (let i = 0; i < scores.length; i++) {
      scoreSum += scores[i];
      if (scoreMax < scores[i]) {
        scoreMax = scores[i];
        scoreMaxGene = this.pool[i];
      }
    }

    const result = {
      scoreAve: scoreSum / scores.length,
      scoreMax,
      scoreMaxGene,
    };
    this.pool = nextGen;

    return result;
  }

  private crossGenes(g1: number[], g2: number[]) {
    const i1 = getRandomInt(0, g1.length);
    const i2 = getRandomInt(0, g1.length);

    const iMin = Math.min(i1, i2);
    const iMax = Math.max(i1, i2);

    const gn: number[] = new Array(g1.length);
    for (let i = 0; i < g1.length; i++) {
      if (i < iMin || i >= iMax) {
        gn[i] = g1[i];
      } else {
        gn[i] = g2[i];
      }
    }

    return gn;
  }

  private mutateGene(g: number[]) {
    const n = Math.round(g.length / 10);
    const ng = g.slice();
    for (let i = 0; i < n; i++) {
      const j = getRandomInt(0, g.length);
      ng[j] = Math.random();
    }
    return ng;
  }
  
  private testGene(gene: number[]) {
    const commandSelector = new CommandSelector(gene);

    const scoreSums: number[] = new Array(this.testSets.length);
    const counts: number[] = new Array(this.testSets.length);
    for (let i = 0; i < this.testSets.length; i++) {
      scoreSums[i] = 0;
      counts[i] = 0;
    }

    for (let i = 0; i < numIteration; i++) {
      const testSetIndex = i % this.testSets.length;
      const testSet = this.testSets[testSetIndex];
      const initialPos = testSet.initialPos;
      const initialDir = testSet.initialDir;
      const field = testSet.field;

      const robot = new Robot(initialPos, initialDir, commandSelector);

      let success = true;
      let score = 0;
      for (let j = 0; j < numTicks; j++) {
        robot.moveOneStep(field);

        const x = robot.pos[0];
        const y = robot.pos[1];
        if (Number.isNaN(y)) {
          success = false;
          break;
        }

        if (x < -0.05 || x > 1.05 || y < -0.05 || y > 1.05) {
          success = false;
          break;
        }

        const t = 1 - j / numTicks;
        score = Math.max(testSet.getScore(t, x, y), score);
      }

      if (success) {
        scoreSums[testSetIndex] += score;
        counts[testSetIndex] += 1;
      }
    }

    let grossScore = 1;
    for (let i = 0; i < this.testSets.length; i++) {
      if (counts[i] === 0) {
        grossScore *= 0;
      } else {
        grossScore *= Math.pow(scoreSums[i] / counts[i], 1.0 / this.testSets.length);
      }
    }

    return grossScore;
  }

  private testGeneAll() {
    let scores: number[] = [];
    for (let gene of this.pool) {
      scores.push(this.testGene(gene));
    }

    return scores;
  }
}