export function getRandomData() {
  const data: number[] = [];
  for (let i = 0; i < 48; i++) {
    data.push(Math.random());
  }

  return data;
}

export function randomSelect(table: number[]) {
  const num = table.length;
  const randomNumber = Math.random();

  let sum = 0;
  for (let i = 0; i < num; i++) {
    sum += table[i];
  }

  let accum = 0;
  for (let i = 0; i < num; i++) {
    accum += table[i];
    
    const probability = accum / sum;
    if (randomNumber <= probability) {
      return i;
    }
  }

  return num - 1;
}

export function getRandomInt(lower: number, upper: number) {
  return Math.floor(Math.random() * (upper - lower)) + lower;
}
